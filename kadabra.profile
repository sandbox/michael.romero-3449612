<?php

/**
 * @file
 * Profile tasks for a Drupal 9 profile.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function kadabra_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'kadabra_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function kadabra_form_install_configure_submit($form, FormStateInterface $form_state) {
  // $site_mail = $form_state->getValue('site_mail');
  // ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}
