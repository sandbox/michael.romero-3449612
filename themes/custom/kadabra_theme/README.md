# Pasos para copiar los templates de paragraph al tema admin

Cuando haya quedado listo el componente en el tema del front y los assets
compilados, ejecutar en la carpeta del tema

- ```node scripts/adminCopyFiles.js ```
- ```node scripts/adminScopeCss.js ```

En caso de tener que ajustar el componente en el tema admin, tocar los
estilos en ```kadabra_admin/assets/css/fixes.css```
