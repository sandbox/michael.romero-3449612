const scope = require('css-scoped')
const fs = require('fs')
const path = require('path');
const postcssUnmq  = require('postcss-unmq')


const data = fs.readFileSync(path.join(__dirname, '..', 'dist/css/main.css'))
const cssContent = data.toString()

async function processPostCSS() {
  return await postcssUnmq.process(cssContent);
}

processPostCSS().then(cleanCSS => {
  var scopedCss = scope(cleanCSS.css, 'lp-builder');
  var dir = path.join(__dirname, '..', '..', 'kadabra_admin/dist/css');

  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }

  fs.writeFile(path.join(__dirname, '..', '..', 'kadabra_admin/dist/css/main.css'), scopedCss, (err) => {
    if (err) throw err;
  });
});

// borra el .map del tema admin
let mapPath = path.join(__dirname, '..', '..', 'kadabra_admin/dist/css/main.css.map');
try {
  if (fs.existsSync(mapPath)) {
    fs.unlinkSync(mapPath)
  }
} catch (error) {
  console.error(error);
}
