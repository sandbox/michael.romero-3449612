const fs = require('fs')
const path = require('path');

fs.cp(path.join(__dirname, '..', 'dist/'), path.join(__dirname, '..', '..', 'kadabra_admin/dist/'), { recursive: true }, (err) => {

  if (err) {
    console.error(err);
  }
});

fs.cp(path.join(__dirname, '..', 'templates/paragraph/'), path.join(__dirname, '..', '..', 'kadabra_admin/templates/paragraph'), { recursive: true }, (err) => {
  if (err) {
    console.error(err);
  }
});

fs.cp(path.join(__dirname, '..', 'templates/field'), path.join(__dirname, '..', '..', 'kadabra_admin/templates/field'), { recursive: true }, (err) => {
  if (err) {
    console.error(err);
  }
});

fs.cp(path.join(__dirname, '..', 'templates/views'), path.join(__dirname, '..', '..', 'kadabra_admin/templates/views'), { recursive: true }, (err) => {
  if (err) {
    console.error(err);
  }
});

fs.cp(path.join(__dirname, '..', 'templates/content'), path.join(__dirname, '..', '..', 'kadabra_admin/templates/content'), { recursive: true }, (err) => {
  if (err) {
    console.error(err);
  }
});
