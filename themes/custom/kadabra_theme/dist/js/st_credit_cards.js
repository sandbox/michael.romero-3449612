
/**
 * @file
 * Tabs component.
 *
 */
(function (Drupal, $, debounce) {

  Drupal.st_credit_cards = {
    credit_cards: [],
  };

  class ST_CreditCards {

    constructor(view) {
      this.view = view;
      this.befForm = this.view.querySelector('.bef-exposed-form');
      this.befFormLinks = Array.from(this.befForm.querySelectorAll('.bef-link'));
      this.befFormActive = this.befFormLinks.find(elem => elem.classList.contains('bef-link--selected'));
      this.segmentsList = this.view.querySelector('.segments-list-selectables');
      this.tabs = this.view.querySelector('.tabs-horizontal');
      this.slickSlider = this.view.querySelector('.slick__slider');
    }

    // add scroll events on mobile to create/delete shadow
    #attachEvents() {
      const links = this.segmentsList.querySelectorAll('.selectable-item-link');
      const self = this;

      links.forEach(elem => {
        elem.addEventListener('click', function (ev) {
          ev.preventDefault();
          const id = this.dataset.termId;
          const toActivate = self.befFormLinks.find(elem => elem.name == id);
          if (!toActivate) return;
          toActivate.click();
        });
      });

      this.tabs.addEventListener('shown.bs.tab', () => {
        if (this.slickSlider) {
          $(this.slickSlider).slick('setPosition');
        }
      });
    }

    #decorateSelectableElement() {
      const toSelected = this.segmentsList.querySelector(`.selectable-item-link[data-term-id="${this.befFormActive?.name}"]`);
      if (!toSelected) return;
      toSelected.classList.add('is-active');
      toSelected.parentElement.classList.add('is-active');
    }

    updateSlickOnResize() {
      if (Drupal.st_breakpoints.breakpoints.current == Drupal.st_breakpoints.MOBILE && this.slickSlider) {
        $(this.slickSlider).slick();
      }
    }

    init() {
      this.#decorateSelectableElement();
      this.#attachEvents();
    }
  }

  Drupal.behaviors.st_credit_cards = {
    attach: function (context, settings) {
      const creditCardsView = context.querySelector('.js-view-credit-cards');

      if (creditCardsView) {
        const instance = new ST_CreditCards(creditCardsView);
        Drupal.st_credit_cards.credit_cards.push(instance);
        instance.init();
      }
    }
  }
})(Drupal, jQuery, Drupal.debounce);
