/**
 * @file
 * Custom utilities.
 *
 */
(function (Drupal, $) {

  'use strict';

  const MOBILE = 'mobile', TABLET = 'tablet', DESKTOP = 'desktop';

  const GRID_BREAKPOINTS = {
    [MOBILE]: 0,
    [TABLET]: 768,
    [DESKTOP]: 992,
  }

  const breakpoints = { current: '', previous: ''};

  Drupal.st_breakpoints = {
    breakpoints,
    MOBILE,
    TABLET,
    DESKTOP
  };

  const breakpoint_event = new CustomEvent('breakpoint-change', {
    detail: {
      breakpoints
    }
  });

  let resize_delay = 300;
  let dropdownActive;

  function debounce(func, wait) {
    let timeout;
    return (...args) => {
      const context = this;
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        timeout = null;
        window.requestAnimationFrame(() => func.apply(context, args));
      }, wait);
    };
  }

  // set current / previos breakpoints on window resize
  function setBreakpoint() {
    const w = window.innerWidth;
    const active = breakpoints.current;

    if (w >= GRID_BREAKPOINTS[DESKTOP] && breakpoints.current != DESKTOP) {
      breakpoints.previous = breakpoints.current;
      breakpoints.current = DESKTOP;
    }

    if (w < GRID_BREAKPOINTS[DESKTOP] && w >= GRID_BREAKPOINTS[TABLET] && breakpoints.current != TABLET) {
      breakpoints.previous = breakpoints.current;
      breakpoints.current = TABLET;
    }

    if (w < GRID_BREAKPOINTS[TABLET] && breakpoints.current != MOBILE) {
      breakpoints.previous = breakpoints.current;
      breakpoints.current = MOBILE;
    }

    if (active != breakpoints.current) {
      window.dispatchEvent(breakpoint_event);
    }
  }

  // NAVBAR FORM COLLAPSE

  // Collapse navbar form
  function collapseNavbarFormCollapse({ c }) {
    let cInstance = bootstrap.Collapse.getInstance(c);
    if (!cInstance) return;
    cInstance.hide();
  }

  // Collapse navbar
  function collapseNavbarCollapse({ c }) {
    let cInstance = bootstrap.Collapse.getInstance(c);
    if (!cInstance) return;
    cInstance.hide();
  }

  // Navbar form collapse click outside
  function checkNavbarFormCollapseClick(ev) {
    return ({ collapsingSearchFormEl }) => {
      if (!collapsingSearchFormEl?.contains(ev.target)) {
        collapseNavbarFormCollapse({ c: collapsingSearchFormEl });
      }
    }
  }

  // NAVBAR NAV && MEGAMENU
  // show always a dropdown in mobile
  function attachNavbarNavEvents({ navbarNav }) {
    navbarNav?.addEventListener('show.bs.dropdown', handleDropdownShow);
    // navbarNav?.addEventListener('hide.bs.dropdown', handleDropdownHide);
  }

  function detachNavbarNavEvents({ navbarNav }) {
    navbarNav?.removeEventListener('show.bs.dropdown', handleDropdownShow);
    // navbarNav?.removeEventListener('hide.bs.dropdown', handleDropdownHide);
  }

  function handleDropdownShow(ev) {
    const prev = dropdownActive
    dropdownActive = ev.target;
    const instance = bootstrap.Dropdown.getInstance(prev);
    if (!instance) return;
    closeNavbarAccordions({ prevDropdown: prev });
    instance.hide();
  }

  function handleDropdownHide(ev) {
    const { target } = ev;
    if (dropdownActive === target) {
      ev.preventDefault();
    }
  }

  function closeNavbarAccordions({ prevDropdown }) {
    const mm = prevDropdown.nextElementSibling;
    const accordionOpen = mm?.querySelector('.accordion-collapse.show');
    const instance = bootstrap.Collapse.getInstance(accordionOpen);
    if (!instance) return;
    instance.hide();
  }


  // TABS SHADOWS
  function setTabsShadow() {
    Drupal.kdb_tabs?.tabs.forEach(tab => {
      tab.toggleTabsShadowDebounced();
    })
  }

  function toggleTabsShadowEvents() {
    Drupal.kdb_tabs?.tabs.forEach(tab => {
      tab.toggleTabsShadowEvents();
    })
  }

  //SLIDERS
  //Slider- Banner Shortcuts
  function bannerShortcutsSlider(elem) {
    if (elem) {
      elem.forEach(el => {
        if (window.innerWidth <= GRID_BREAKPOINTS[TABLET]) {
          const esHorizontal = el.classList.contains('horizontal');
          const items = esHorizontal ? 1 : 2;

          let slidesItem = el.querySelectorAll("li");
          if (slidesItem.length > 2) {
            $(el).owlCarousel({
              items: items,
              dots: true,
              loop: true,
              margin: 6,
              nav: false,
              autoplay: false,
              autoplayTimeout: 5000,
              autoplayHoverPause: true,
            });
          }
        } else {
          $(el).owlCarousel('destroy');
        }
      });
    }
  }

  //Responsive Tables
  function responsiveTables() {
    var tables = document.querySelectorAll('table');
    for (var i = 0; i < tables.length; i++) {
      var table = tables[i];
      var tableWidth = table.offsetWidth;
      var containerWidth = table.parentElement.offsetWidth;

      if (tableWidth > containerWidth) {
        table.classList.add('responsive-table');
      } else {
        table.classList.remove('responsive-table');
      }
    }
  }

  function toggleSliderPrevNextSliderButtons() {
    Drupal.st_gallery_slider?.sliders.forEach(slide => {
      slide.toggleSliderPrevNextSliderButtons();
    })
  }

  function updateInfoWindowsPosition() {
    Drupal.st_maps?.maps.forEach(map => {
      map.updateInfoWindowPosition();
    })
  }

  function updateCreditCardsSlickOnResize() {
    Drupal.st_credit_cards?.credit_cards.forEach(view => {
      view.updateSlickOnResize();
    })
  }

  // SLICK LIGHTBOX CHANGE

  Drupal.behaviors.kadabra = {
    attach: function (context, settings) {
      const [html] = once('init', 'html', context);
      const bodyEl = html?.querySelector('body');

      const navbarHeight = getComputedStyle(document.body).getPropertyValue('--navbarHeight').replace('px', '');
      const navbarTopHeight = getComputedStyle(document.body).getPropertyValue('--navbarTopHeight').replace('px', '');
      const toolbarHeight = getComputedStyle(document.body).getPropertyValue('--toolbarHeight').replace('px', '');
      const scrolledPosition = Number(navbarHeight) + Number(toolbarHeight) + Number(navbarTopHeight);

      let position = window.scrollY;

      // Navbar Main & Collapse
      const [navbarMain] = once('navbar-main', '#navbar-main', context);

      const [navbarCollapse] = once('collapseNavbar', '.js-navbar-collapse', context);
      const navbarNav = navbarCollapse?.querySelector('.navbar-nav');

      // Search Collapse
      const [collapsingSearchFormEl] = once('collapseSearchForm', '.js-navbar-form', context);

      // Accordions Footer
      const [accordionFooterEls] = once('accordionFooter', '.js-accordion-footer-menu', context);

      //SLIDERS
      //Icons
      let arrowLeft='<svg class="icon"><use xlink:href="#arrow-slider-left"></use></svg>';
      let arrowRight='<svg class="icon"><use xlink:href="#arrow-slider-right"></use></svg>';

      //Slider- Banner Hero
      const bannerHeroSliderEls = once('slider', '.bannerHero__slider', context);
      if (bannerHeroSliderEls){
        bannerHeroSliderEls.forEach(el => {
          let slidesItem = el.querySelectorAll(".item");
          if (slidesItem.length > 1){
            $(el).owlCarousel({
              dots: true,
              items: 1,
              loop: true,
              margin: 5,
              nav: true,
              autoplay: true,
              autoplayTimeout: 5000,
              autoplayHoverPause: true,
              navText: [
                arrowLeft,
                arrowRight
              ],
            });
          }
          else{
            el.classList.add("disabled");
          }
        })
      }

      //Slider- Basic Highlight
      const basicHighlightSliderEls = once('slider', '.basicHighlight .owl-carousel', context);
      if (basicHighlightSliderEls){
        basicHighlightSliderEls.forEach(el => {
          let slidesItem = el.querySelectorAll(".item");
          if (slidesItem.length > 3){
            el.classList.remove('disabled');
            $(el).owlCarousel({
              dots: true,
              loop: true,
              rewind: true,
              autoplay: true,
              autoplayTimeout: 5000,
              autoplayHoverPause: true,
              navText: [
                arrowLeft,
                arrowRight
              ],
              responsive: {
                0: {
                  items: 1,
                  margin: 24,
                  nav: false,
                  stagePadding: 35,
                },
                570: {
                  items: 3,
                  margin: 24,
                  nav: true,
                  stagePadding: 0,
                },
              }
            });
          }
          else{
            el.classList.add("disabled");
          }
        })
      }

      //Slider- List Highlight
      const listHighlightSliderEls = once('slider', '.listHighlight .owl-carousel', context);
      if (listHighlightSliderEls){
        listHighlightSliderEls.forEach(el => {
          let slidesItem = el.querySelectorAll(".item");
          if (slidesItem.length > 3){
            el.classList.remove('disabled');
            $(el).owlCarousel({
              dots: true,
              loop: true,
              rewind: true,
              autoplay: true,
              autoplayTimeout: 5000,
              autoplayHoverPause: true,
              navText: [
                arrowLeft,
                arrowRight
              ],
              responsive: {
                0: {
                  items: 1,
                  margin: 8,
                  nav: false,
                  stagePadding: 35,
                },
                570: {
                  items: 3,
                  margin: 8,
                  nav: true,
                  stagePadding: 0,
                },
              }
            });
          }
          else{
            el.classList.add("disabled");
          }
        })
      }

      //Slider- Banner Shortcuts
      const bannerShortcutsSliderEls = once('slider', '.bannerShortcuts .owl-carousel', context);

      // NAVBAR SEARCHFORM EVENTS
      if (collapsingSearchFormEl) {
        collapsingSearchFormEl.addEventListener('show.bs.collapse', ev => {
          bodyEl?.classList.add('show-backdrop');
          const navbarCollapseInstance = bootstrap.Collapse.getInstance(navbarCollapse);
          if (navbarCollapseInstance) {
            navbarCollapseInstance.hide();
          }
        });

        collapsingSearchFormEl.addEventListener('hide.bs.collapse', ev => {
          bodyEl?.classList.remove('show-backdrop');
        });
      }

      // NAVBAR / MEGAMENU  EVENTS
      if (navbarCollapse) {

        const navbarNavMegaMenus = navbarCollapse.querySelectorAll('.navbar-nav-megamenu');
        const menuAccordions = navbarCollapse.querySelectorAll('.accordion-main-menu');
        const menuAccordionsCollapse = navbarCollapse.querySelectorAll('.accordion-main-menu .accordion-collapse');

        const dropdownToggles = navbarCollapse.querySelectorAll('.navbar-nav .dropdown-toggle');
        const dropdownToggleFirst = navbarCollapse.querySelector('.navbar-nav .dropdown-toggle');

        ['hidden.bs.dropdown', 'show.bs.dropdown'].forEach(evtName => {
          navbarNav.addEventListener(evtName, ev => {
            // prevent close mega menu when another dropdown is open
            const isOtherDropdownActive = Array.from(navbarNav.querySelectorAll('.dropdown-toggle.show')).length;
            const isNavbarCollapseOpen = navbarCollapse.classList.contains('show');

            if (ev.type.includes('hidde') && !isOtherDropdownActive && (!isNavbarCollapseOpen && breakpoints.current == DESKTOP)) {
              bodyEl?.classList.remove('show-backdrop', 'megamenu-is-open');
            } else {
              bodyEl?.classList.add('show-backdrop', 'megamenu-is-open');
            }
          })
        });

        // prevent events to bubble up and close navbar collapse
        ['hide.bs.collapse', 'hidden.bs.collapse', 'show.bs.collapse', 'shown.bs.collapse'].forEach(evtName => {
          menuAccordions.forEach(acc => {
            acc.addEventListener(evtName, ev => {
              ev.stopPropagation();
            });
          })
        });

        // prevent megamenu close when click inside.
        navbarNavMegaMenus.forEach(m => {
          m.addEventListener('click', ev => {
            ev.stopPropagation();
          });
        });

        // show first dropdown when navbar is open
        if (dropdownToggleFirst) {
          const dropdownInstance = bootstrap.Dropdown.getOrCreateInstance(dropdownToggleFirst);
          if (!dropdownInstance) return;

          // show first item when navbar is open
          // navbarCollapse.addEventListener('shown.bs.collapse', ev => {
          //   if (dropdownInstance) {
          //     dropdownInstance.show();
          //   }
          // });
        }

        // show / hide body backdrop
        navbarCollapse.addEventListener('show.bs.collapse', ev => {
          bodyEl?.classList.add('show-backdrop', 'megamenu-is-open');
        });

        navbarCollapse.addEventListener('hidden.bs.collapse', ev => {
          // prevent removing backdrop when search form is open
          // const searchFormCollapseOpen = collapsingSearchFormEl?.classList.contains('show');
          // if (!searchFormCollapseOpen) {
            bodyEl?.classList.remove('show-backdrop', 'megamenu-is-open');
          // }

          // close accordions & dropdowns when menu is closed.
          const menuAccordionCollapseArr = Array.from(menuAccordionsCollapse);
          const accordionOpen = menuAccordionCollapseArr.find(a => a.classList.contains('show'));
          const dropdownOpen = Array.from(dropdownToggles).find(a => a.classList.contains('show'));

          if (accordionOpen) {
            const instance = bootstrap.Collapse.getOrCreateInstance(accordionOpen);
            instance.hide();
          }

          if (dropdownOpen) {
            const instance = bootstrap.Dropdown.getInstance(dropdownOpen);
            instance.hide();
          }
        });
      }

      // FOOTER ACCORDION COLLAPSE EVENTS
      if (accordionFooterEls) {
        // Toggle footer accordion class to change bg color
        ['hide.bs.collapse', 'show.bs.collapse'].forEach(evtName => {
          accordionFooterEls.addEventListener(evtName, ev => {
            const { target } = ev;
            if (!target) return;
            const parentCollapse = target.parentElement;
            if (ev.type.includes('show')) {
              parentCollapse.classList.add('show');
            }
            if (ev.type.includes('hide')) {
              parentCollapse.classList.remove('show');
            }
          });
        });
      }

      // WINDOW EVENTS
      if (html) {
        screen?.orientation?.addEventListener('change', debounce(setBreakpoint, resize_delay));
        window.addEventListener('orientationchange', debounce(setBreakpoint, resize_delay));

        const debounceResize = debounce(() => {
          setBreakpoint();
          setTabsShadow();

        }, resize_delay);

        window.addEventListener('resize', debounceResize, { passive: true });

        window.addEventListener('breakpoint-change', (e) => {
          if (breakpoints.current == DESKTOP) {
            // bodyEl?.classList.remove('show-backdrop');
            collapseNavbarFormCollapse({ c: collapsingSearchFormEl });
            collapseNavbarCollapse({ c: navbarCollapse });
          }

          if (breakpoints.current == MOBILE) {
            bodyEl.classList.remove('show-backdrop', 'megamenu-is-open');
          }

          if (navbarCollapse) {
            if (breakpoints.current == DESKTOP) {
              detachNavbarNavEvents({ navbarNav });
            } else {
              attachNavbarNavEvents({ navbarNav });
            }
          }

          bannerShortcutsSlider(bannerShortcutsSliderEls);
          toggleTabsShadowEvents();
          toggleSliderPrevNextSliderButtons();
          responsiveTables();
          updateInfoWindowsPosition();
          updateCreditCardsSlickOnResize();
        }, false);

        // Include barrio.js logic to prevent scroll on swipe sections
        window.addEventListener('scroll', function() {
          if (bodyEl?.classList.contains('in-swipe-section')) return;
          const scroll = window.scrollY;

          if (scroll > position) {
            document.querySelector('body').classList.add("scrolldown");
            document.querySelector('body').classList.remove("scrollup");
          } else {
            document.querySelector('body').classList.add("scrollup");
            document.querySelector('body').classList.remove("scrolldown");
          }

          position = scroll;

          if (scroll > scrolledPosition) {
            bodyEl?.classList.add("has-scrolled");
          }
          else {
            bodyEl?.classList.remove("has-scrolled");
          }

        }, { passive: true });

        // Barrio Click
        document.addEventListener('click', function (event) {

          // If the clicked element doesn't have the right selector, bail
          if (!event.target.matches('.dropdown-item a.dropdown-toggle')) return;

          // Don't follow the link
          event.preventDefault();

          toggle(event.target.next('ul'));
          event.stopPropagation();

        }, false);

        document.addEventListener('click', (ev) => {
          // const checkCollapse = checkNavbarFormCollapseClick(ev);
          // checkCollapse({ collapsingSearchFormEl });
        });

        setBreakpoint();
      }
    }
  };

})(Drupal, jQuery);
