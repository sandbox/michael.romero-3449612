
/**
 * @file
 * Custom utilities.
 *
 */
(function (Drupal, $) {
  console.log(Drupal);
  // Register ScrollTrigger plugin
  let body = document.body;
  gsap.registerPlugin(ScrollTrigger);

  // onEnter - forward past "start" (typically when trigger is scrolled into view)
  // onLeave - forward past "end" (typically when trigger is scrolled out of view)
  // onEnterBack - backward past "end" (typically when trigger is scrolled back into view)
  // onLeaveBack - backward past "start" (typically when trigger goes backward out of view)
  // onUpdate - every time the scroll position changes while between "start" and "end".
  // onToggle - passes the start or end in either direction
  // onRefreshInit - immediately before measurements are recalculated (typically on resize)
  // onRefresh - after measurements are recalculated (typically on resize)

  // Gsap Code
  let currentIndex = -1;
  let animating;

  // panels
  let swipePanels = gsap.utils.toArray(".swipe-hero .swipe-hero-item");
  // wrappers
  let outerWrappers = gsap.utils.toArray(".swipe-hero .swipe-hero-item-outer");
  let innerWrappers = gsap.utils.toArray(".swipe-hero .swipe-hero-item-inner");
  let swipeHeadings = gsap.utils.toArray(".swipe-hero-heading");
  // content
  let swipeCopies = gsap.utils.toArray(".swipe-hero-copy");
  // swipeCtas
  let swipeCtas = gsap.utils.toArray(".swipe-hero-cta");
  // media
  let swipeMedias = gsap.utils.toArray(".swipe-hero-item-media");
  // Pagination
  let swipPanelsPager = gsap.utils.toArray(".swipe-hero-pagination");
  let swipPanelsPagerItems = gsap.utils.toArray(".swipe-hero-pagination .swipe-hero-pagination-item");

  // set second panel two initial 100%
  gsap.set(".x-100", { yPercent: 30, autoAlpha: 0 });

  gsap.fromTo(swipeHeadings[0], {yPercent: 110, opacity: 0}, { yPercent: 0, opacity: 1, duration: .5, ease: "none" });
  gsap.fromTo(swipeCopies[0], {yPercent: 110, opacity: 0}, { yPercent: 0, opacity: 1, duration: .5, ease: "none" }, .45);
  gsap.fromTo(swipeMedias[0], {scale: .85, opacity: 0}, { scale: 1, opacity: 1, duration: .45, ease: "power2.inOut" });
  gsap.fromTo(swipeCtas[0], {autoAlpha: 0}, {autoAlpha: 1, duration: .8, ease: "none" }, 1.35);

  // set z-index levels for the swipe panels
  gsap.set(swipePanels, {
    zIndex: i => i
  });

  gsap.set(outerWrappers, { yPercent: 100 });
  gsap.set(innerWrappers, { yPercent: -100 });

  // create an observer and disable it to start
  let intentObserver = ScrollTrigger.observe({
    type: "wheel,touch",
    onUp: () => !animating && gotoPanel(currentIndex + 1, true),
    onDown: () => !animating && gotoPanel(currentIndex - 1, false),
    wheelSpeed: -1, // to match mobile behavior, invert the wheel speed
    tolerance: 10,
    preventDefault: true,
    onPress: self => {
      // on touch devices like iOS, if we want to prevent scrolling, we must call preventDefault() on the touchstart (Observer doesn't do that because that would also prevent side-scrolling which is undesirable in most cases)
      ScrollTrigger.isTouch && self.event.preventDefault()
    }
  })
  intentObserver.disable();

  let preventScroll = ScrollTrigger.observe({
    preventDefault: true,
    type: "wheel,scroll",
    allowClicks: true,
    onEnable: self => self.savedScroll = self.scrollY(), // save the scroll position
    onChangeY: self => self.scrollY(self.savedScroll)    // refuse to scroll
  });

  preventScroll.disable();


  // handle the panel swipe animations
  function gotoPanel(index, isScrollingDown) {
    animating = true;

    let tl = gsap.timeline({
      defaults: { duration: 1.2, ease: "expo.inOut" },
      onComplete: () => {
        animating = false;
      }
    });

    // return to normal scroll if we're at the end or back up to the start
    if (
      (index === swipePanels.length && isScrollingDown) ||
      (index === -1 && !isScrollingDown)
    ) {

      if (!isScrollingDown && index === -1) {
        tl.to(swipPanelsPagerItems[index + 1], { "--scale-bar": 0 }, .2);
      }

      currentIndex = index;
      intentObserver.disable();
      preventScroll.disable();
      animating = false;
      // now make it go 1px beyond in the correct direction so that it doesn't trigger onEnter/onEnterBack.
      preventScroll.scrollY(preventScroll.scrollY() + (index === swipePanels.length ? 1 : -1));
      body.classList.remove("in-swipe-section");
      return;
    } else {
      body.classList.add("in-swipe-section");
    }

    //   target the second panel, last panel?

    const direction = isScrollingDown ? 1 : -1;

    let target = isScrollingDown ? swipePanels[index] : swipePanels[currentIndex];

    let currentPanel = swipePanels[index];

    let isDarkBg = currentPanel?.classList.contains("bg-blue") || currentPanel?.classList.contains("bg-indigo");
    let bgColor = isDarkBg ? '#fff' : '#092265';
    let color = isDarkBg ? '#fff' : '#3f3f3f';

    tl
      .to(target, {
        yPercent: isScrollingDown ? 0 : 30,
        autoAlpha: isScrollingDown ? 1 : 0,
      })
      .to(swipPanelsPager, {
        "--bg-color": bgColor,
        "--color": color,
      },
        0
      )
      .fromTo(
        [outerWrappers[index], innerWrappers[index]],
        { yPercent: (i) => (i ? -100 * direction : 100 * direction) },
        { yPercent: 0 },
        0
      )

    // animate after first child
    if ((isScrollingDown && currentIndex > -1) || (!isScrollingDown && currentIndex < swipePanels.length)) {
      tl
        .fromTo(
          swipeHeadings[index],
          {
            yPercent: 110,
            opacity: 0,
          },
          {
            yPercent: 0,
            opacity: 1,
            duration: .8,
            ease: "none"
          },
          0
        )
        .fromTo(
          swipeCopies[index],
          {
            yPercent: 110,
            opacity: 0,
          },
          {
            yPercent: 0,
            opacity: 1,
            duration: .5,
            ease: "none"
          },
          .75
        )
        .fromTo(
          swipeMedias[index],
          {
            scale: .75,
            opacity: 0,
          },
          {
            scale: 1,
            opacity: 1,
            duration: .35,
            ease: "power2.inOut"
          },
          .5
        )
        .fromTo(
          swipeCtas[index],
          {
            // "--width": 0,
            autoAlpha: 0
          },
          {
            // "--width": 100,
            autoAlpha: 1,
            duration: .8,
            ease: "none"
          },
          1.45
        );
    }

    currentIndex = index;

    // Fill
    if (isScrollingDown) {
      tl.to(swipPanelsPagerItems[index],
        {
          "--scale-bar": 1,
        },
        .2
      );
    } else {
      tl.to(swipPanelsPagerItems[index + 1],
        {
          "--scale-bar": 0
        },
        .2
      );
    }
  }

  // pin swipe section and initiate observer
  ScrollTrigger.create({
    trigger: ".swipe-hero",
    pin: true,
    anticipatePin: false,
    start: "top top",
    end: "bottom bottom",
    markers: false,
    onEnter: (self) => {
      if (preventScroll.isEnabled === false) {
        self.scroll(self.start);
        preventScroll.enable();
        intentObserver.enable();

        gotoPanel(currentIndex + 1, true);
      }
    },
    onEnterBack: (self) => {
      // console.log('onEnterBack', preventScroll.isEnabled, currentIndex);
      if (preventScroll.isEnabled === false) {
        self.scroll(self.start);
        preventScroll.enable();
        intentObserver.enable();

        gotoPanel(currentIndex - 1, false);
      }
    },
  });

  // prevent mobile ios address bar generate jumps
  ScrollTrigger.normalizeScroll(true);
  // ScrollTrigger.normalizeScroll({
  //   allowNestedScroll: true,
  //   lockAxis: false,
  //   momentum: self => Math.min(3, self.velocityY / 1000), // dynamically control the duration of the momentum when flick-scrolling
  //   type: "touch,wheel,pointer", // now the page will be drag-scrollable on desktop because "pointer" is in the list
  // });


  Drupal.behaviors.kdb_swipe_sections = {
    attach: function (context, settings) {
      console.log('loaded kdw swipe sections');
    }
  };

})(Drupal, jQuery);
