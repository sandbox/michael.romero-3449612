/**
 * @file
 * Video component.
 *
 */
(function (Drupal, $, debounce) {

  Drupal.st_videos = {
    videos: [],
  };

  class ST_Video {
    #play;
    #video;

    set #setPlay(el) {
      this.#play = el;
    }

    set #setVideo(el) {
      this.#video = el;
    }

    constructor(video) {
      this.videoContainer = video;
      this.#setPlay = this.videoContainer.querySelector('.btn-play');
      this.#setVideo = this.videoContainer.querySelector('video');
      this.#video.controls = false;
    }

    #attachEvents() {
      this.#play.addEventListener('click', ev => {
        this.videoContainer.classList.add('playing');
        this.#video.play();
        this.#video.controls = true;
      });

      this.#video.addEventListener('ended', ev => {
        this.#video.controls = false;
        this.videoContainer.classList.remove('playing');
      });
    }

    init() {
      this.#attachEvents();
    }
  }


  Drupal.behaviors.st_video = {
    attach: function (context, settings) {
      const videosEl = once('st-video', '.js-video-custom', context);

      videosEl.forEach(videoEl => {
        const video = new ST_Video(videoEl);
        Drupal.st_videos.videos.push(video);
        video.init();
      })
    }
  }
})(Drupal, jQuery, Drupal.debounce);
