(function ($, Drupal) {
  Drupal.behaviors.showResponseMessage = {
    attach: function (context, settings) {

      var $paragraphs = $('.paragraph--type--te-resulto-util', context);

      $paragraphs.each(function () {
        var $paragraph = $(this);
        var $responseMessage = $paragraph.find('.response-message');
        var $webformContent = $paragraph.find('.webform-content');
        var $optionNo = $paragraph.find('.option-no');
        var $optionSi = $paragraph.find('.option-si');

        // Función para manejar el cambio de estado de los botones.
        function handleButtonClick($button, $otherButton, $contentToShow, $contentToHide) { 
          $otherButton.removeClass('is-active');
          $button.addClass('is-active');
          $contentToHide.hide();
          $contentToShow.fadeIn();
        }

        // Escucha el click en el botón de "Sí" y muestra el mensaje de respuesta.
        $optionSi.on('click', function (ev) {
          ev.preventDefault();
          handleButtonClick($(this), $optionNo, $responseMessage, $webformContent);
        });

        // Escucha el click en el botón de "No" y muestra el formulario.
        $optionNo.on('click', function (ev) {
          ev.preventDefault();
          handleButtonClick($(this), $optionSi, $webformContent, $responseMessage);
        });
      });
    }
  };
})(jQuery, Drupal);
