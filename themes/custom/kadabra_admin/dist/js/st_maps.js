
/**
 * @file
 * Map View component.
 * Used by Sitios de Interes / Beneficios
 *
 */
(function (Drupal, $, debounce) {
  const MARKER_ICON_DEFAULT = '/profiles/kadabra/themes/custom/kadabra_theme/geofieldmap_icons/marker-default.svg';
  const MARKER_ICON_ACTIVE  = '/profiles/kadabra/themes/custom/kadabra_theme/geofieldmap_icons/marker-active.svg';
  let INFOWINDOW_POSITION = '';

  Drupal.st_maps = {
    maps: [],
  };

  class ST_Maps {

    constructor(map, settings, context) {
      this.ctx = context;
      this.mapView = map;

      this.segmentsList = this.mapView.querySelector('.segments-list-selectables');
      // segments bef links
      this.befForm = this.mapView.querySelector('.bef-exposed-form');
      this.befFormLinks = Array.from(this.befForm?.querySelectorAll('.bef-link') || []);
      this.befFormActive = this.befFormLinks.filter(elem => elem.classList.contains('bef-link--selected'));

      // bootstrap selects

      this.bootstrapSelects = Array.from(this.befForm?.querySelectorAll('.js-form-item-field-payment-methods-value, .js-form-item-field-state-target-id') || []);

      // map point of attention list
      this.listMapElems = this.mapView.querySelector('.list-map');
      this.listMapItemsElems = Array.from(this.listMapElems?.querySelectorAll('.list-map-item') || []);

      // map related elems
      this.mapAttachedView = this.mapView.querySelector('.js-map-view-map-elem');
      this.mapEl = this.mapAttachedView?.querySelector('.geofield-google-map');
      this.mapId = this.mapEl?.id;
      this.mapDrupal = settings['geofield_google_map'][this.mapId];
      this.mapDrupalSettings = this.mapDrupal?.map_settings;
      this.infoWindowRef = null;
      this.markers = {};
    }

    #attachEvents() {
      const links = this.segmentsList?.querySelectorAll('.selectable-item-link') || [];
      const self = this;

      links.forEach(elem => {
        elem.addEventListener('click', function (ev) {
          ev.preventDefault();
          const id = this.dataset.termId;
          const toActivate = self.befFormLinks.find(elem => elem.name == id);
          if (!toActivate) return;
          toActivate.click();
        });
      });

      if (!this.mapDrupal) return;

      $(this.ctx).on('geofieldMapInit', ev => {
        console.log(ev);
        this.#initMarkersEvents();
        this.#initInfowindowEvents();
      });

    }

    #updateInfoWindowPosition() {
      INFOWINDOW_POSITION = Drupal.st_breakpoints.breakpoints.current == Drupal.st_breakpoints.MOBILE ?
      { pixelOffset: new google.maps.Size(0, 270) } :
      { pixelOffset: new google.maps.Size(0, 390) };
      this.infoWindowRef?.setOptions(INFOWINDOW_POSITION);
    }

    #initInfowindowEvents() {
      const { map: { infowindow }, markers } = this.mapDrupalSettings;
      this.infoWindowRef = infowindow;

      const listItemWithIcons = this.listMapItemsElems.filter(node => node.dataset.markerIcon);

      listItemWithIcons.forEach(node => {
        const { markerId, markerIcon } = node.dataset;
        markers[markerId].default_icon = markerIcon;
        markers[markerId].icon = markerIcon;
        markers[markerId].setIcon(markerIcon);
      });

      this.#updateInfoWindowPosition();

      google.maps.event.addListener(this.infoWindowRef, 'domready', () => {
        const infoWindowElem = this.mapEl.querySelector('.gm-style-iw.gm-style-iw-c');
        infoWindowElem.classList.add('st-infowindow');
      });

      google.maps.event.addListener(this.infoWindowRef, 'closeclick', (ev) => {
        this.#resetMarkersIcons({ markers });
        this.#resetPointOfAttentionClass();
      });
    }

    #initMarkersEvents() {
      const { markers } = this.mapDrupalSettings;
      for (let [key, marker] of Object.entries(markers)) {
        google.maps.event.addListener(marker, 'click', () => {
          this.mapDrupalSettings.map.setCenter(marker.getPosition());
          this.#resetMarkersIcons({ markers });
          this.#setPointOfInterestActive({ key, marker })
        });
      }
    }

    #resetPointOfAttentionClass() {
      this.listMapItemsElems.forEach(point => point.classList.remove('is-active'));
    }

    #resetMarkersIcons({ markers }) {
      Object.keys(markers).forEach(k => {
        markers[k].setIcon(markers[k].default_icon ?? MARKER_ICON_DEFAULT);
      });
    }

    #setPointOfInterestActive({ key, marker }) {
      this.#resetPointOfAttentionClass();

      marker.setIcon(MARKER_ICON_ACTIVE);

      const toActivate = this.listMapItemsElems.find(point => point.dataset.markerId === key);
      this.listMapElems.scrollTop = toActivate?.offsetTop;
      toActivate?.classList.add('is-active');
    }

    #decorateSelectableElement() {
      this.befFormActive.forEach(befActive => {
        const toSelected = this.segmentsList?.querySelector(`.selectable-item-link[data-term-id="${befActive?.name}"]`);
        if (!toSelected) return;
        toSelected.classList.add('is-active');
        toSelected.parentElement.classList.add('is-active');
      })
    }

    #initBootstrapSelects() {
      const options = {
        showTick: true
      };

      this.bootstrapSelects.forEach(selectWrapper => {
        const label = selectWrapper.getElementsByTagName('label')[0];
        let title = Drupal.t('Choose an option');
        if (label) {
          title = label.textContent;
        }
        const select = selectWrapper.querySelector('select');

        if (select.value) {
          selectWrapper.classList.add('is-filled');
        }

        $(select).selectpicker({
          ...title,
          noneSelectedText: title,
          options
        });
        // this.#attachBootstrapSelectsEvents();
      });

    }

    #attachBootstrapSelectsEvents() {
      this.bootstrapSelects.forEach(selectWrapper => {
        const select = selectWrapper.querySelector('select');
        $(select).on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
          console.log(clickedIndex, isSelected, previousValue);
          if (isSelected) {
            selectWrapper.classList.add('is-filled');
          }
        });
      });
    }

    init() {
      this.#decorateSelectableElement();
      this.#initBootstrapSelects();
      this.#attachEvents();
    }

    updateInfoWindowPosition() {
      this.#updateInfoWindowPosition();
    }
  }


  Drupal.behaviors.st_maps = {
    attach: function (context, settings) {
      const mapsElem = once('st-maps', '.js-kadabra-map-view', context) || [];
      // Drupal.st_maps.maps = [];

      mapsElem.forEach(elem => {
        const instance = new ST_Maps(elem, settings, context);
        instance.init();
        Drupal.st_maps.maps.push(instance);
      })
    }
  }
})(Drupal, jQuery, Drupal.debounce);
