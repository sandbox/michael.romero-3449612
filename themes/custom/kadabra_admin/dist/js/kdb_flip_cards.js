/**
 * @file
 * Custom utilities.
 *
 */
(function (Drupal, $) {
  Drupal.behaviors.kdbFlipCards = {
    attach: function (context, settings) {
      const flipCardsElems = once('flip-cards', '.js-flip-cards', context);

      if (flipCardsElems.length) {
        flipCardsElems.forEach(function (flipCards) {
          const buttons = flipCards.querySelectorAll('.flip-card-button');
          buttons.forEach(function (button) {
            button.addEventListener('show.bs.collapse', function (ev) {
              if (
                Drupal.st_breakpoints.breakpoints.current == Drupal.st_breakpoints.DESKTOP
              ) {
                ev.preventDefault();
              }
            });
          });
        });
      }
    }
  }
})(Drupal, jQuery);
