/**
 * @file
 * Gallery Slider component.
 *
 */
(function (Drupal, $, debounce) {

  Drupal.st_gallery_slider = {
    sliders: [],
  };

  class ST_Gallery {
    #slick;
    #paginationInfo;
    #sliderInner;
    #sliderNavbar;
    #sliderNavbarFraction;

    set #setSlick(el) {
      this.#slick = el;
    }

    get slick() {
      return this.#slick;
    }

    set #setPaginationInfo(el) {
      this.#paginationInfo = el;
    }

    get paginationInfo() {
      return this.#paginationInfo;
    }

    set #setSliderInner(el) {
      this.#sliderInner = el;
    }

    get sliderInner() {
      return this.#sliderInner;
    }

    set #setSliderNavbar(el) {
      this.#sliderNavbar = el;
    }

    get sliderNavbar() {
      return this.#sliderNavbar;
    }

    set #setSliderNavbarFraction(el) {
      this.#sliderNavbarFraction = el;
    }

    get sliderNavbarFraction() {
      return this.#sliderNavbarFraction;
    }

    constructor(el) {
      this.slickLightbox = el;
      this.#sliderInner = this.slickLightbox.querySelector('.slick-lightbox-inner');
    }

    #attachEvents() {
      const self = this;

      $('.slick-lightbox').on('init', function (event, slick, currentSlide, nextSlide) {
        self.#initSlickElements({ slick });
      });

      $('.slick-lightbox').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        if (!self.#slick) {
          self.#initSlickElements({ slick });
        }
        let i = (currentSlide ? currentSlide : 0) + 1;
        self.#sliderNavbarFraction.textContent = (i + '/' + slick.slideCount);
      });

    }

    #initSlickElements({ slick }) {
      this.#slick = slick;
      const $slidesEl = slick?.$slides;
      const total = slick?.slideCount;

      $slidesEl.each((idx, slide) => {
        const text = `${idx + 1} / ${total}`;
        const inner = slide.querySelector('.slick-lightbox-slick-item-inner');
        const div = this.#createPaginationInfo({ text });
        inner.prepend(div);
      });

      this.#createSliderNavbar();
    }

    #createPaginationInfo({ text }) {
      const divEl = document.createElement('div');
      divEl.classList.add('slick-pagination');
      divEl.textContent = text;
      return divEl;
    }

    #createSliderNavbar() {
      const slickInner = this.slickLightbox.querySelector('.slick-lightbox-inner');

      const navEl = document.createElement('nav');
      navEl.classList.add('slick-pagination-mobile');

      const fractionEl = document.createElement('div');
      fractionEl.classList.add('slick-pagination-fraction');

      this.#sliderNavbarFraction = fractionEl;
      this.#sliderNavbar = navEl;

      navEl.appendChild(fractionEl);
      slickInner.appendChild(navEl);

      if (Drupal.st_breakpoints.breakpoints.current != Drupal.st_breakpoints.DESKTOP) {
        const prevButton = this.#slick.$prevArrow;
        const nextButton = this.#slick.$nextArrow;

        navEl.prepend(prevButton[0]);
        navEl.append(nextButton[0]);
      }
    }

    // toggle tabs events depending on viewport
    toggleSliderPrevNextSliderButtons() {
      const prevButton = this.#slick.$prevArrow;
      const nextButton = this.#slick.$nextArrow;
      const slickLightboxSlick = this.slickLightbox.querySelector('.slick-lightbox-slick');

      if (Drupal.st_breakpoints.breakpoints.current != Drupal.st_breakpoints.DESKTOP) {
        this.#sliderNavbar.prepend(prevButton[0]);
        this.#sliderNavbar.append(nextButton[0]);
      } else {
        slickLightboxSlick.prepend(prevButton[0]);
        slickLightboxSlick.append(nextButton[0]);
      }
    }

    init() {
      // this.#createPaginationInfo();
      this.#attachEvents();
    }
  }


  Drupal.behaviors.st_gallery_slider = {
    attach: function (context, settings) {
      const [slickLightBox] = once('st-lightbox', '.slick-lightbox', context);

      if (slickLightBox) {
        const slickEl = new ST_Gallery(slickLightBox);
        slickEl.init();
        Drupal.st_gallery_slider.sliders.push(slickEl);
      }
    }
  }
})(Drupal, jQuery, Drupal.debounce);
