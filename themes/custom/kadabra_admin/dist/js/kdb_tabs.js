
/**
 * @file
 * Tabs component.
 *
 */
(function (Drupal, $, debounce) {

  Drupal.kdb_tabs = {
    tabs: [],
  };

  class Kdb_Tabs {
    #navTab;

    #navDots;

    set #setNavTab(el) {
      this.#navTab = el;
    }

    set #setNavDots(el) {
      this.#navDots = el;
    }

    constructor(tab) {
      this.tab = tab;
      this.#setNavTab = this.tab.querySelector('.js-nav-tabs');
      this.#setNavDots = this.tab.querySelector('.js-nav-tabs-dots');
      this.toggleTabsShadowDebounced = debounce(() => this.#toggleTabsShadow(), 50);
    }

    // check if tabs needs shadows depending on scroll
    #toggleTabsShadow() {
      const wrapper = this.#navTab.parentElement;
      const { scrollLeft, scrollWidth, clientWidth } = this.#navTab;
      const hasScroll = scrollWidth > clientWidth;
      const isScrolled = scrollLeft > 0;
      const isFullScroll = (clientWidth + scrollLeft) === scrollWidth;

      if (Drupal.st_breakpoints.breakpoints.current != Drupal.st_breakpoints.MOBILE) return;

      if (hasScroll) {
        wrapper.classList.add('end-shadow');
      } else {
        wrapper.classList.remove('end-shadow');
      }

      if (isScrolled) {
        wrapper.classList.add('start-shadow');
      } else {
        wrapper.classList.remove('start-shadow');
      }

      if (isFullScroll && hasScroll) {
        wrapper.classList.remove('end-shadow');
      }
    }

    // sync dots to trigger tab change
    #syncTabsShadowDots() {
      this.#navDots.addEventListener('click', ev => {
        ev.preventDefault();

        const { target } = ev;
        if (target.nodeName.toLowerCase() != 'a') return;

        const navLink = this.#navTab.querySelector(`.nav-link[href="${target.hash}"]`);
        const navLinkX = navLink.offsetLeft;
        const navTabInstance = bootstrap.Tab.getOrCreateInstance(navLink);

        if (!navTabInstance) return;
        navTabInstance.show();
        this.#navTab.scrollLeft = navLinkX;
      });

      this.#navTab.addEventListener('shown.bs.tab', ev => {
        const { hash: current } = ev.target;
        const { hash: previous } = ev.relatedTarget;

        const previousDot = this.#navDots.querySelector(`.nav-link-dot[href="${previous}"]`);
        const currentDot = this.#navDots.querySelector(`.nav-link-dot[href="${current}"]`);

        if (!previousDot || !currentDot) return;

        previousDot.classList.remove('active');
        currentDot.classList.add('active');
      });
    }

    // add scroll events on mobile to create/delete shadow
    #attachScrollEvents() {
      const self = this;
      this.#navTab.addEventListener('scroll', self.toggleTabsShadowDebounced);
    }

    // remove scroll events on mobile to create/delete shadow
    #detachScrollEvents() {
      const self = this;
      this.#navTab.removeEventListener('scroll', self.toggleTabsShadowDebounced);
    }

    // toggle tabs events depending on viewport
    toggleTabsShadowEvents() {
      if (Drupal.st_breakpoints.breakpoints.current == Drupal.st_breakpoints.MOBILE) {
        this.#toggleTabsShadow();
        this.#attachScrollEvents();
      } else {
        this.tab.classList.remove('end-shadow', 'start-shadow');
        this.#detachScrollEvents();
      }
    }

    init() {
      this.toggleTabsShadowEvents();

      if (this.#navDots) {
        this.#syncTabsShadowDots();
      }
    }
  }


  Drupal.behaviors.kdb_tabs = {
    attach: function (context, settings) {
      // Tabs Shadows
      const tabsShadowEl = once('st-shadows', '.js-kdb-tabs-shadow', context);

      tabsShadowEl.forEach(tabEl => {
        const tab = new Kdb_Tabs(tabEl);
        Drupal.kdb_tabs.tabs.push(tab);
        tab.init();
      })
    }
  }
})(Drupal, jQuery, Drupal.debounce);
