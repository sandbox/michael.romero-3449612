<?php

namespace Drupal\kadabra_tools;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
class KadabraFileinfoTwigExtension extends AbstractExtension {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new KadabraFileinfoTwigExtension object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Declare your custom twig extension here.
   *
   * @return array|\TwigFunction[]
   *   Default return value
   */
  public function getFunctions() {
    return [
      new TwigFunction('getFileInfo', [$this, 'getFileInfo'], ['is_safe' => ['html']]),
    ];
  }

  /**
   * Process archivos_descargables paragraph.
   *
   * Gets the extension and size of the file associated with the paragraph.
   *
   * @param int $file_id
   *   Id of the file.
   *
   * @return array|null
   *   Array with the file extension and size or null if the paragraph does not.
   */
  public function getFileInfo(int $file_id) {
    $media_entity = $this->entityTypeManager->getStorage('media')->load($file_id);
    // Verify if the media entity has an associated file.
    if ($media_entity->hasField('field_media_document') && !$media_entity->get('field_media_document')->isEmpty()) {
      // Get the file entity associated with the media entity.
      $file_entity = $media_entity->get('field_media_document')->entity;
      // Get the extension and size of the file.
      $file_extension = pathinfo($file_entity->getFilename(), PATHINFO_EXTENSION);
      $file_size = $this->formatFileSize($file_entity->getSize());
      $variables['file_extension'] = $file_extension;
      $variables['file_size'] = $file_size;
      return $variables;
    }
    return NULL;
  }

  /**
   * Format the file size.
   *
   * @param int $size
   *   File size in bytes.
   *
   * @return string
   *   Formatted file size.
   */
  private function formatFileSize($size) {
    $units = ['B', 'KB', 'MB', 'GB', 'TB'];
    for ($i = 0; $size >= 1024 && $i < 4; $i++) {
      $size /= 1024;
    }
    return round($size, 2) . $units[$i];
  }

}
