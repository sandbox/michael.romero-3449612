<?php

namespace Drupal\kadabra_tools\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Controller for custom tools.
 */
class KadabraToolsController extends ControllerBase {
  /**
   * Drupal extension module list service.
   *
   * @var Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The list of available modules.
   */
  public function __construct(ModuleExtensionList $extension_list_module) {
    $this->extensionListModule = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module')
    );
  }

  /**
   * Render custom twig with welcome message.
   */
  public function welcome() {
    $module_path = $this->extensionListModule->getPath('kadabra_tools');
    $img = $module_path . '/img/welcome.png';
    return [
      '#theme' => 'kadabra_welcome',
      '#title' => 'Welcome to Kadabra',
      '#img' => $img,
    ];
  }

}
